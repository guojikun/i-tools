import { createRouter, createWebHistory } from 'vue-router'
import Root from '../views/root.vue'
import plugins from './modules/plugins.js'
import settings from './modules/settings.js'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'root',
      component: Root,
      redirect: '/home',
      children: [
        {
          path: '/home',
          name: 'home',
          component: () => import('@/views/HomeView.vue')
        }
      ]
    },
    settings,
    plugins
  ]
})

export default router
