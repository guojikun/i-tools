export default {
    path: '/settings',
    redirect: '/settings/profile',
    component: () => import('@/views/settings/index.vue'),
    children: [
      {
        path: 'profile',
        name: 'settings.profile',
        component: () => import('@/views/settings/prefile.vue')
      },
    ]
  }