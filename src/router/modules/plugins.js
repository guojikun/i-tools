export default 
    {
        path: '/plugins',
        name: 'plugins',
        component: () => import('@/views/plugins/index.vue'),
        redirect: '/plugins/installed',
        children: [
            {
                path: 'installed',
                name: 'plugins.installed',
                component: () => import('@/views/plugins/installed.vue')
            },
            {
                path: 'available',
                name: 'plugins.available',
                component: () => import('@/views/plugins/available.vue')
            },
        ]
    }
